# Tech and Croissant: Creating a Kubeflow Component

## Introduction
Greetings! This repository contains an almost ready to launch pipeline. Please help us by building the last component and pushing it to the cloud! <br>
This repository is part of the Tech & Croissant where participants are expected to learn how to write small pipeline components.<br>
When using this repository outside of the T&C, please keep in mind that the cloud permissions might have changed which might stop you from pushing the pipeline to the cloud.<br>
If so, please contact Oliver for support.


## Story
In the last meeting you and your team discussed the final steps needed to complete a pipeline for a POC for a client.<br>
As a data scientist, you and the other members were occupied with building the best model for this specific task while the data engineer worked on composing the pipeline.<br>
In the middle of the meeting, one of your mates proposes to add another step in the pipeline. Your team member thinks, that this might further boost the perfomance of your model.<br>
As the projects first iteration is approaching its deadline and the teams data engineer is not available, you decide to build the component yourself.<br>
At the first look, you understand close to nothing of what is happening. After a while you notice the more than strange coding style that is used for building a component.<br>
You notice that you only need to write a component and change three lines in the pipeline code cell.


## Instructions
1. Pull this repository to your computer.
2. Write a component that drops empty rows in a dataframe.
3. Include your component in the pipeline.
4. Run the pipeline.
5. Enjoy your pipeline in the cloud! :)<br>

The component must expect the input in the form of an artifact and it must return an artifact.

### Bonus
The pipeline is currently logging every step of execution.
Add a metric that tracks how many rows were dropped by your component!
